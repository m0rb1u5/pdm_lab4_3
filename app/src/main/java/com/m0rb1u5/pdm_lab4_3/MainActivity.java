package com.m0rb1u5.pdm_lab4_3;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
   private Button btnSinHilos;
   private Button btnHilo;
   private Button btnAsyncTask;
   private Button btnCancelar;
   private Button btnAsyncTaskProgressDialog;
   private Button btnIntentService;
   private ProgressBar pbarProgreso;
   private ProgressDialog pDialog;
   private MiTareaAsincronaDialog tarea2;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      btnSinHilos = (Button) findViewById(R.id.btnSinHilos);
      btnHilo = (Button) findViewById(R.id.btnHilo);
      btnAsyncTask = (Button) findViewById(R.id.btnAsyncTask);
      btnCancelar = (Button) findViewById(R.id.btnCancelar);
      btnAsyncTaskProgressDialog = (Button) findViewById(R.id.btnAsyncTaskProgressDialog);
      btnIntentService = (Button) findViewById(R.id.btnIntentService);
      pbarProgreso = (ProgressBar) findViewById(R.id.pbarProgreso);

      btnSinHilos.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            pbarProgreso.setMax(100);
            pbarProgreso.setProgress(0);
            for(int i = 1; i<=10; i++) {
               tareaLarga();
               pbarProgreso.incrementProgressBy(10);
            }
            Toast.makeText(MainActivity.this, "Tarea finalizada!", Toast.LENGTH_LONG).show();
         }
      });

      btnHilo.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            new Thread(new Runnable() {
               @Override
               public void run() {
                  pbarProgreso.post(new Runnable() {
                     @Override
                     public void run() {
                        pbarProgreso.setMax(100);
                        pbarProgreso.setProgress(0);
                     }
                  });
                  for(int i = 1; i<=10; i++) {
                     tareaLarga();
                     pbarProgreso.post(new Runnable() {
                        @Override
                        public void run() {
                           pbarProgreso.incrementProgressBy(10);
                        }
                     });
                  }
                  runOnUiThread(new Runnable() {
                     @Override
                     public void run() {
                        Toast.makeText(MainActivity.this, "Tarea finalizada", Toast.LENGTH_LONG).show();
                     }
                  });
               }
            }).start();
         }
      });

      final MiTareaAsincrona[] miTareaAsincrona = {null};

      btnAsyncTask.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            miTareaAsincrona[0] = new MiTareaAsincrona();
            miTareaAsincrona[0].execute();
         }
      });

      btnCancelar.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            miTareaAsincrona[0].cancel(true);
         }
      });

      btnAsyncTaskProgressDialog.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(true);
            pDialog.setMax(100);

            tarea2 = new MiTareaAsincronaDialog();
            tarea2.execute();
         }
      });

      btnIntentService.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Intent msgIntent = new Intent(MainActivity.this, MyIntentService.class);
            msgIntent.putExtra("iteraciones",10);
            startService(msgIntent);
         }
      });

      IntentFilter filter = new IntentFilter();
      filter.addAction(MyIntentService.ACTION_PROGRESO);
      filter.addAction(MyIntentService.ACTION_FIN);
      ProgressReceiver rcv = new ProgressReceiver();
      registerReceiver(rcv, filter);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   private void tareaLarga() {
      try {
         Thread.sleep(1000);
      }
      catch (InterruptedException error) {
         error.printStackTrace();
         Log.e("Error", error.toString());
      }
   }

   private class MiTareaAsincrona extends AsyncTask<Void, Integer, Boolean> {
      @Override
      protected Boolean doInBackground(Void... params) {
         for (int i = 1; i <= 10; i++) {
            tareaLarga();
            publishProgress(i*10);
            if (isCancelled()) {
               break;
            }
         }
         return true;
      }

      @Override
      protected void onProgressUpdate(Integer... values) {
         int progreso = values[0].intValue();
         pbarProgreso.setProgress(progreso);
      }

      @Override
      protected void onPreExecute() {
         pbarProgreso.setMax(100);
         pbarProgreso.setProgress(0);
      }

      @Override
      protected void onPostExecute(Boolean aBoolean) {
         if(aBoolean) {
            Toast.makeText(MainActivity.this, "Tarea finalizada!", Toast.LENGTH_SHORT).show();
         }
      }

      @Override
      protected void onCancelled() {
         Toast.makeText(MainActivity.this, "Tarea cancelada!", Toast.LENGTH_SHORT).show();
      }
   }

   private class MiTareaAsincronaDialog extends AsyncTask <Void, Integer, Boolean> {
      @Override
      protected Boolean doInBackground(Void... params) {
         for(int i=1; i<=10; i++) {
            tareaLarga();
            publishProgress(i*10);
            if(isCancelled()) {
               break;
            }
         }
         return true;
      }

      @Override
      protected void onProgressUpdate(Integer... values) {
         int progreso = values[0].intValue();
         pDialog.setProgress(progreso);
      }

      @Override
      protected void onPreExecute() {
         pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
               MiTareaAsincronaDialog.this.cancel(true);
            }
         });
         pDialog.setProgress(0);
         pDialog.show();
      }

      @Override
      protected void onPostExecute(Boolean aBoolean) {
         if(aBoolean) {
            pDialog.dismiss();
            Toast.makeText(MainActivity.this, "Tarea finalizada!", Toast.LENGTH_SHORT).show();
         }
      }

      @Override
      protected void onCancelled() {
         Toast.makeText(MainActivity.this, "Tarea cancelada!", Toast.LENGTH_SHORT).show();
      }
   }

   public class ProgressReceiver extends BroadcastReceiver {
      @Override
      public void onReceive(Context context, Intent intent) {
         if (intent.getAction().equals(MyIntentService.ACTION_PROGRESO)) {
            int prog = intent.getIntExtra("progreso", 0);
            pbarProgreso.setProgress(prog);
         }
         else if (intent.getAction().equals(MyIntentService.ACTION_FIN)) {
            Toast.makeText(MainActivity.this, "Tarea finalizada!", Toast.LENGTH_SHORT).show();
         }
      }
   }
}
